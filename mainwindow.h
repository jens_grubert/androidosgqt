#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>

#include "GraphicsWindowQt.h"
#include "OsgMainApp.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QObject
{
    Q_OBJECT

public:
    explicit MainWindow(const QRect & pRect, QObject *parent = 0);
    ~MainWindow();
    osgQt::GraphicsWindowQt* createGraphicsWindow( int x, int y, int w, int h, const std::string& name="", bool windowDecoration=false );

public slots:
    void updateOsg() ;

private:
    Ui::MainWindow *ui;
    OsgMainApp mMainApp ;
    osgQt::GLWidget * mMainWidget;
    QTimer mFrequencyTimer;
    osgQt::GraphicsWindowQt* mGw;

};

#endif // MAINWINDOW_H
