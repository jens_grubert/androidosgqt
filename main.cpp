#include "mainwindow.h"
#include <QApplication>
#include <QDesktopWidget>
#include <QRect>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w(a.desktop()->screenGeometry());

    return a.exec();
}
