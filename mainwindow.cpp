#include "mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(const QRect & pRect, QObject *parent) :
    QObject(parent)
{

    //osgQt::initQtWindowingSystem();

    mGw = createGraphicsWindow(0,0, pRect.width(), pRect.height());

    mMainApp.initOsgWindow(0,0,pRect.width(),pRect.height());

    mMainApp.loadObject("/sdcard/cow.osg");

    osg::Vec4 tVec(0.0f, 1.0f, 0.0f, 0.0f);
    mMainApp.setClearColor(tVec);

    mMainWidget = mGw->getGLWidget() ;
    mMainWidget->show();
    mMainWidget->makeCurrent();

    // Connects the timeout signal raised by frequency timer to the main task manager.
    connect( &mFrequencyTimer, SIGNAL( timeout() ), this, SLOT( updateOsg() ));
    mFrequencyTimer.setInterval(100);
    mFrequencyTimer.start();

}

MainWindow::~MainWindow()
{
    mFrequencyTimer.stop();
}

osgQt::GraphicsWindowQt* MainWindow::createGraphicsWindow( int x, int y, int w, int h, const std::string& name, bool windowDecoration)
{
    osg::DisplaySettings* ds = osg::DisplaySettings::instance().get();
    osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
    traits->windowName = name;
    traits->windowDecoration = windowDecoration;
    traits->x = x;
    traits->y = y;
    traits->width = w;
    traits->height = h;
    traits->doubleBuffer = true;
    traits->alpha = ds->getMinimumNumAlphaBits();
    traits->stencil = ds->getMinimumNumStencilBits();
    traits->sampleBuffers = ds->getMultiSamples();
    traits->samples = ds->getNumMultiSamples();

    return new osgQt::GraphicsWindowQt(traits.get());
}

void MainWindow::updateOsg()
{
    mMainApp.draw();
    mGw->swapBuffers();
}
