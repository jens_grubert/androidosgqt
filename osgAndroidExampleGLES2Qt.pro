#-------------------------------------------------
#
# Project created by QtCreator 2014-04-02T10:35:04
#
#-------------------------------------------------

OSG_DIR="c:/openscenegraph/svn/OpenSceneGraph/build-android-opengles2"
OSG_LIBDIR=$$OSG_DIR/lib/armeabi-v7a/release


QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = osgAndroidExampleGLES2Qt
TEMPLATE = app

INCLUDEPATH += $$OSG_DIR/include

LIBS += \
"-L$$OSG_LIBDIR" -losgdb_dds -losgdb_openflight -losgdb_tga -losgdb_rgb -losgdb_osgterrain -losgdb_osg -losgdb_ive -losgdb_deprecated_osgviewer -losgdb_deprecated_osgvolume -losgdb_deprecated_osgtext -losgdb_deprecated_osgterrain -losgdb_deprecated_osgsim -losgdb_deprecated_osgshadow -losgdb_deprecated_osgparticle -losgdb_deprecated_osgfx -losgdb_deprecated_osganimation -losgdb_deprecated_osg -losgdb_serializers_osgvolume -losgdb_serializers_osgtext -losgdb_serializers_osgterrain -losgdb_serializers_osgsim -losgdb_serializers_osgshadow -losgdb_serializers_osgparticle -losgdb_serializers_osgmanipulator -losgdb_serializers_osgfx -losgdb_serializers_osganimation -losgdb_serializers_osg -losgViewer -losgVolume -losgTerrain -losgText -losgShadow -losgSim -losgParticle -losgManipulator -losgGA -losgFX -losgDB -losgAnimation -losgUtil -losg -lOpenThreads \




SOURCES += main.cpp\
        mainwindow.cpp \
        GraphicsWindowQt.cpp \
        OsgAndroidNotifyHandler.cpp \
        OsgMainApp.cpp


HEADERS  += mainwindow.h\
        GraphicsWindowQt.h \
        OsgAndroidNotifyHandler.hpp \
        OsgMainApp.hpp \
        Export.h

FORMS    += mainwindow.ui

CONFIG += mobility
MOBILITY = 

